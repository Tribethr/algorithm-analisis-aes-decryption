package main;

import decryption.Trainer;
import utilities.Subset;

public class Main {
    public static void main(String[] args) {
        String encryptedString = "xZwM7BWIpSjYyGFr9rhpEa+cYVtACW7yQKmyN6OYSCv0ZEg9jWbc6lKzzCxRSSIvOvlimQZBMZOYnOwiA9yy3YU8zk4abFSItoW6Wj0ufQ0=";
        String key = "29dh120_dk1_3";
        Trainer trainer = new Trainer(encryptedString, key);
        trainer.train(100, 10, 10);
    }
}
