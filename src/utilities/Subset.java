package utilities;

import lib.IConstants;

import java.util.ArrayList;

public class Subset implements IConstants {

	private ArrayList<Character> alphabet;
	private ArrayList<Character> numbers;
	private float probability;

	public Subset(int size){
		scramble(size);
	}

	public Subset(ArrayList<Character> alphabet, ArrayList<Character> numbers){
		this.numbers = numbers;
		this.alphabet = alphabet;
	}

	private void clearSets(){
		alphabet = new ArrayList<>();
		numbers = new ArrayList<>();
	}

	public void scramble(int size){
		clearSets();
		int alphabetSize = Randoms.randInt(1, size-1);
		for(int alphabetIndex = 0; alphabetIndex < alphabetSize; alphabetIndex++){
			Character selectedChar = ALPHABET[Randoms.randInt(0, ALPHABET.length-1)];
			if(alphabet.indexOf(selectedChar) == -1) {
				alphabet.add(selectedChar);
			}else {
				--alphabetIndex;
			}
		}
		int numberSize = size-alphabetSize;
		for(int numberIndex = 0; numberIndex < numberSize; numberIndex++){
			Character selectedNumber = NUMBERS[Randoms.randInt(0, NUMBERS.length-1)];
			if(numbers.indexOf(selectedNumber) == -1) {
				numbers.add(selectedNumber);
			}else {
				--numberIndex;
			}
		}
	}

	private boolean setHasKey(ArrayList<Character> set, char value){
		for(Character letter: set){
			if(letter == value){
				return true;
			}
		}
		return false;
	}

	public boolean hasLetter(char letter){
		return setHasKey(alphabet, letter);
	}

	public boolean hasNumber(char number){
		return setHasKey(numbers, number);
	}

	private ArrayList<Character> getSetIntersection(ArrayList<Character> set1, ArrayList<Character> set2){
		ArrayList<Character> ret = new ArrayList<>();
		for (Character character : set1) {
			if (setHasKey(set2, character)) {
				ret.add(character);
			}
		}
		return ret;
	}

	public Subset intersection(Subset subset){
		return new Subset(getSetIntersection(alphabet, subset.getAlphabet()), getSetIntersection(numbers, subset.getNumbers()));
	}

	public int lenght(){
		return alphabet.size() + numbers.size();
	}

	public int combinations(){
		return alphabet.size() * numbers.size();
	}

	public ArrayList<Character> getAlphabet(){
		return alphabet;
	}

	public ArrayList<Character> getNumbers(){
		return numbers;
	}

	public void setProbability(float probability){
		this.probability = probability;
	}
	
	public float getProbability(){
		return probability;
	}

	@Override
	public String toString(){
		return "Probality: "+String.valueOf(getProbability())+"\tAlphabet: "+alphabet+"\t Numbers: "+numbers;
	}
}