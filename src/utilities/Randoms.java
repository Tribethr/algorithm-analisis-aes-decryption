package utilities;

import java.util.Random;

public class Randoms {
    private static Random random = new Random();

    public static int randInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }
}
