package utilities;

import java.util.Comparator;

public class SortByProbability implements Comparator<Subset> {
    @Override
    public int compare(Subset sub1, Subset sub2) {
        return (int)(sub2.getProbability() - sub1.getProbability());
    }
}
