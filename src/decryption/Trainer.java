package decryption;

import aes.AES;
import utilities.SortByProbability;
import utilities.Subset;

import java.util.ArrayList;

public class Trainer {

    private final byte FIRST_POS = 7;
    private final byte LAST_POS = 11;

    private StringBuilder key;
    private Decrypter decrypter;
    private SortByProbability sortByProbability;

    public Trainer(String encryptedString, String key){
        this.key = new StringBuilder(key);
        decrypter = new Decrypter(encryptedString);
        sortByProbability = new SortByProbability();
    }

    private Subset findPossibleKeys(Subset selectedSubset){
        float acceptedKeys = 0;
        for (char letter : selectedSubset.getAlphabet()) {
            key.setCharAt(FIRST_POS, letter);
            for (char number : selectedSubset.getNumbers()) {
                key.setCharAt(LAST_POS, number);
                if (decrypter.decrypt(key.toString()) != null) {
                    acceptedKeys++;
                }
            }
        }
        float probability = acceptedKeys / selectedSubset.combinations() * 100;
        probability = Float.isNaN(probability)? 0 : probability;
        selectedSubset.setProbability(probability);
        return selectedSubset;
    }

    private ArrayList<Subset> getNewPopulation(int populationSize, int subsetSize, ArrayList<Subset> alive){
        ArrayList<Subset> ret = new ArrayList<>(alive);
        int newIndividuals = populationSize-alive.size();
        for(int individual = 0; individual < newIndividuals; individual++){
            ret.add(findPossibleKeys(new Subset(subsetSize)));
        }
        return ret;
    }

    private ArrayList<Subset> getAlives(ArrayList<Subset> population){
        ArrayList<Subset> alives = new ArrayList<>();
        for (int individual = 0; individual+1 < population.size() && population.get(individual+1).getProbability() > 0; individual+=2) {
            Subset firstParent = population.get(individual);
            Subset secondParent = population.get(individual+1);
            Subset newChild = findPossibleKeys(firstParent.intersection(secondParent));
            if(newChild.getProbability() < firstParent.getProbability()){
                alives.add(firstParent);
            }else if(newChild.getProbability() < secondParent.getProbability()){
                alives.add(secondParent);
            }else{
                alives.add(newChild);
            }
        }
        if(alives.size() == 0){
            alives.add(population.get(0));
        }
        return alives;
    }

    public void train(int epochs, int subsetSize, int populationSize) {
        ArrayList<Subset> alives = new ArrayList<>();
        for(int epoch = 0; epoch < epochs; epoch++){
            ArrayList<Subset> population = getNewPopulation(populationSize, subsetSize, alives);
            population.sort(sortByProbability);
            alives = getAlives(population);
            System.out.println(alives);
            if(alives.get(0).getProbability() == 100){
                System.out.println("Train end at " + (epoch + 1) + " epochs");
                return;
            }
        }
    }

}