package decryption;

import aes.AES;

public class Decrypter {

	private String encryptedString;
    
	
	public Decrypter(String encryptedString) {
		this.encryptedString = encryptedString;
	}
	
	public String decrypt(String key) {
		return AES.decrypt(encryptedString, key);
	}
	
}
